import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.updateEmojis = [];
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    //SET
    this.emojis = emojis;

    //DISPLAY
    let emojiSelector = document.querySelector("#emojis")
    let displayEmojis = document.createElement('p')
    displayEmojis.setAttribute("class", "emoji-display")
    emojiSelector.append(displayEmojis)
    displayEmojis.textContent = this.updateEmojis;
  }

  addBananas() {
    this.updateEmojis = this.emojis.map(()=>this.emojis + this.banana);
  }
}
